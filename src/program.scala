import java.util

import scala.io.BufferedSource

object program {

	def main (args: Array[String]) {
		val nameSet: util.TreeSet[String] = new util.TreeSet[String]()
		val srcFirstName: BufferedSource = scala.io.Source.fromFile(args(0))
		val srcLastName: BufferedSource = scala.io.Source.fromFile(args(1))

		var data = srcFirstName.getLines mkString "\n"
		data.toLowerCase.split("\n").map(i => nameSet.add(i))
		data = srcLastName.getLines mkString "\n"
		data.toLowerCase.split("\n").map(i => nameSet.add(i))

		val N = new NameExtractor(nameSet)
		val names = N.extractName("ahmedaliahsan@gmail.com")
		names.toArray.foreach(println)
		/* Remove subsets and shortest overlapping names */
	}
}
