import java.util

class NameExtractor(nameList: util.TreeSet[String]) {
  val nameSet: util.TreeSet[String] = nameList

  def validateEmail(email: String): Boolean =
    if("""(?=[^\s]+)(?=(\w+)@([\w\.]+))""".r.findFirstIn(email).isEmpty)false else true

  def extractName(email: String): util.LinkedList[String] = {
    val nameList = new util.LinkedList[String]()
    if(validateEmail(email)){
      val userName = email.substring(0,email.indexOf("@"))
      for(i <- 0 to userName.length){
        for(j <- i to userName.length){
          val innerName = userName.substring(i,j)
          if(innerName.length > 2){
            if(nameSet.contains(innerName)){
              nameList.add(innerName)
              // update i for non overlapping names
            }
          }
        }
      }

    } else {
      throw new IllegalArgumentException("Invalid email: " + email)
    }
    nameList
  }
}
